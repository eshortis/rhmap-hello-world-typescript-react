
import * as React from 'react';

export default class Header extends React.Component<undefined, undefined> {
  render () {
    return (
      <header className='header-footer-bg'>
        <div className='center'>
          <h3>Red Hat Mobile <small>QuickStart - React</small></h3>
        </div>
      </header>
    );
  }
}
